import React, { Component } from "react";
import * as Survey from "survey-react";

import "survey-react/survey.css";

Survey.StylesManager.applyTheme("default");

// creating the SurveyComponent
class SurveyComponent extends Component {
    constructor(props) {
        super(props);

    }
    render() {

        const json = this.props.json; // obtains the input from the provided argument that you pass to the component in index.js

        const survey = new Survey.Model(json); // using the survey module from survey-react

        // function that prints the result to the console upon completion of the survey
        survey.onComplete.add(function (sender, options) {
            console.log(JSON.stringify(sender.data)); // change here to push to your DB
        });

        // output that will be rendered
        return (
            <Survey.Survey
                model={survey}
            />
        );
    }
}

export default SurveyComponent;
